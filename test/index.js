const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const logger = require('../index')
const api = express()
const router = express.Router()
api.use(cors())
api.use(bodyParser.json())
api.use(bodyParser.urlencoded({ extended: true }))

// Setup logger ----------------------------------------------------------------
api.use(
    logger({
        formatContent: { 'text/html; charset=utf-8': content => `TEXT: ${content}`, 'image/jpeg': 'THIS IS AN IMAGE' }
    })
)

// Setup routes ----------------------------------------------------------------
api.get('/json', (request, response) => {
    response.json({ fname: 'amir', lname: 'naderi', age: 27 })
})

api.get('/text', (request, response) => {
    response.send('Hello World :D')
})

api.get('/image', (request, response) => {
    response.sendFile('./image.jpg', { root: '.' })
})

//------------------------------------------------------------------------------

const PORT = 6000
api.listen(PORT, () => console.log(`Server is running on port ${PORT}`))
