const fs = require('fs')
const path = require('path')
const isValidPath = require('is-valid-path')
const shell = require('shelljs')

let logDirAndFile = ({ logPath, fileName }) => {
    let logDir =
        typeof logPath === 'function'
            ? logPath(
                  {
                      timestamp: new Date().getTime(),
                      path: path.posix.join(request.baseUrl || '', request.path || ''),
                      method: request.method,
                      headers: request.headers,
                      query: request.query
                  },
                  { headers: response.getHeaders(), status: response.statusCode }
              )
            : (typeof logPath === 'string' || typeof logPath === 'number') && isValidPath(logPath)
            ? logPath
            : './log'

    let logFileName =
        typeof fileName === 'function'
            ? fileName(
                  {
                      timestamp: new Date().getTime(),
                      path: path.posix.join(request.baseUrl || '', request.path || ''),
                      method: request.method,
                      headers: request.headers,
                      query: request.query
                  },
                  { headers: response.getHeaders(), status: response.statusCode }
              )
            : (typeof fileName === 'string' || typeof fileName === 'number') && !/[\\/:*?"<>|]+/i.test(fileName)
            ? fileName
            : new Date().toISOString().split('T')[0]

    if (!fs.existsSync(logDir)) shell.mkdir('-p', logDir)
    return { logDir, logFileName }
}

let defaultFormatter = (content, contentType = '') => {
    if (content.length > 1000) return '[LARGE_CONTENT]'
    if (/application\/json/gim.test(contentType)) return JSON.parse(content)
    else if (!contentType || /(text|html)/gim.test(contentType)) return content
    else return '[NO_FORMATTER_PROVIDED]'
}

let _formatContent = (content, contentType = '', formatContent) => {
    if (formatContent) {
        if (typeof formatContent === 'function') {
            return formatContent(content, contentType)
        }
        if (typeof formatContent === 'object' && formatContent[contentType]) {
            if (typeof formatContent[contentType] === 'function') {
                return formatContent[contentType](content)
            }
            return formatContent
        }
    }
    return defaultFormatter(content, contentType)
}

module.exports = function ({ logPath, fileName, delimiter, formatContent }) {
    return function (request, response, next) {
        let { logDir, logFileName } = logDirAndFile({ logPath, fileName })
        let oldWrite = response.write
        let oldEnd = response.end
        let chunks = []
        response.write = function (chunk) {
            chunks.push(Buffer.from(chunk))

            oldWrite.apply(response, arguments)
        }
        response.end = function (chunk) {
            if (chunk) chunks.push(Buffer.from(chunk))
            let responseBody = Buffer.concat(chunks).toString('utf8')

            fs.appendFileSync(
                path.join(logDir, logFileName + '.log'),
                JSON.stringify(
                    {
                        timestamp: new Date().getTime(),
                        datetime: new Date().toISOString().replace(/T/, ' ').replace(/-/g, '/').slice(0, -1),
                        path: path.posix.join(request.baseUrl || '', request.path || ''),
                        method: request.method,
                        headers: request.headers,
                        query: request.query,
                        body: _formatContent(request.body, request.headers['content-type'], formatContent),
                        response: {
                            headers: response.getHeaders(),
                            status: response.statusCode,
                            body: _formatContent(responseBody, response.getHeader('content-type'), formatContent)
                        }
                    },
                    null,
                    4
                ) + (delimiter || '\n\n')
            )

            oldEnd.apply(response, arguments)
        }

        next()
    }
}
